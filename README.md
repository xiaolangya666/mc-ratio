# @FEKIT/MC-RATIO
一个移动端大小屏自适应方案支持插件,可以横向自适应和横竖双向自适应，包括根字号全局适配和标签区域内局部自适应。并且可以开启DPR让网页UI达到原生APP的细腻程度


#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)


#### 演示
[http://fekit.asnowsoft.com/plugins/mc-ratio/](http://fekit.asnowsoft.com/plugins/mc-ratio/)


#### 开始
下载项目: 

```git
git clone https://gitlab.com/fekits/mc-ratio.git
```

```
npm i @fekit/mc-ratio

```

#### 参数
```
    {
        el   : 'id',               {DomObject}   DOM对象，如果不填则默认为HTML根标签
        size : [750, 1334],        {Number}      设计稿的尺寸
        full : true | false,       {Boolean}     是否开启全屏模式？如果开启开屏模式则对宽度和高度双向自适应 *
        dpr:   true | false        {Boolean}     开启DPR功能,开启DPR后网页尺寸将设置为设备物理分辨率，网页1px可以达到原生APP细腻程度
        fixed: 2                   {Number}      设置字号精度为小数点后2，一般建议为2位
        then : function () {}      {Function}    自适应字号改变时的回调
    }
```

#### 示例

建议使用方式：
```html
<!--复制这一段在<head>即可 STA-->
<script>
    !function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):(e=e||self,e.mcRatio=t())}(this,function(){"use strict";return function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},t=document,i=t.documentElement,n=t.getElementsByTagName("head")[0],o=t.getElementsByName("viewport")[0];o||((o=t.createElement("meta")).name="viewport",n.appendChild(o));var d=e.el||i,a=e.size[0],c=e.size[1]/a,l=e.full,m=function(){if(d===i){var t=window.devicePixelRatio,n=e.dpr?1/t:1;o.setAttribute("content","width=device-width,initial-scale=1,minimum-scale=".concat(n,",maximum-scale=").concat(n,",user-scalable=no"))}var m,s=i.clientWidth,f=i.clientHeight;m=(m=l&&c>f/s?f/c/a:s/a).toFixed(e.fixed?e.fixed+2:2),d.style.fontSize=100*m+"px",e.then&&e.then(100*m)};m(),window.addEventListener("resize",m)}});
    // size:[设计稿宽度，设计稿高度] full:ture开启宽度双向自适应，false仅开启宽度自适应
    mcRatio({size: [750, 1334], full: 1, dpr:1, fixed:2});
    // 写尺寸规则：所有尺寸写设计稿尺寸／100，比如设计稿宽600px写width:6rem,设计稿字号28px写fint-size:.28rem;
</script>
<!--复制这一段在<head>即可 END-->
```

全局自适应：
```javascript
// 模块化方式,以下实例没有设置full为true，则默认不开启高度自适应，仅对宽度做自适应缩放，这种一般用于有滚动条的页面。同时也不开启DPR功能
import mcRatio from '@fekit/mc-ratio';
mcRatio({
        size:  [750, 1334],                  // 设计稿的尺寸
});
```

局部自适应
```javascript
// 以下示例仅对指定ID为area的元素做自适应，需要注意的是，如果你仅对网页中的部分区域做自适应，请勿开启dpr功能，该功能将影响全局网页
mcRatio({
    el:    document.getElementId('area'),    // 为ID为area的标签设置自适应字号，当字号不是设置在HTML根标签时，只能在区域内用em方案自适应
    size:  [750, 1334],                      // 设计稿的尺寸
    fixed: 2
});
```

#### 版本
```
v1.0.2 [Latest version]
1、修复局部自适应开启DPR影响全局的BUG，修改为局部自适应设置DPR无效。
```

```
v1.0.1
1、判断<head>中是否有<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">,如果没有则自动创建一个
```

```
v1.0.0
1、新增了dpr功能，开启了DPR功能后，可以自动设置设备尺寸为物理分辨率使网页达到原生APP精细效果。就是解决经典的1px太粗问题
2、新增了fixed参数可以设置字号缩放时的小数字精度。
3、实现了横向自适应和横竖双向自适应，并且竖向自适应可自由设定阀值。
4、可以设置根字号全局适配和标签区域内局部自适应。
```

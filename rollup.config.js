import path from 'path';
import resolve from 'rollup-plugin-node-resolve';                        // 帮助寻找node_modules里的包
import babel from 'rollup-plugin-babel';                                 // rollup 的 babel 插件，ES6转ES5
import replace from 'rollup-plugin-replace';                             // 替换待打包文件里的一些变量，如 process在浏览器端是不存在的，需要被替换
import commonjs from 'rollup-plugin-commonjs';                           // 将非ES6语法的包转为ES6可用
import uglify from 'rollup-plugin-uglify-es';                            // 压缩JS

const env = process.env.NODE_ENV;

let config = {
  input: path.resolve(__dirname, './lib/mc-ratio.js'),
  output: [
    {
      file: path.resolve(__dirname, './npm/mc-ratio.umd.js'),
      format: 'umd',
      name: 'mcRatio'
    },
    {
      file: path.resolve(__dirname, './npm/mc-ratio.esm.js'),
      format: 'esm'
    }
  ],
  plugins: [
    resolve({
      mainFields: [
        'module',
        'jsnext:main',
        'main'
      ]
    }),
    babel({
      exclude: '**/node_modules/**'
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(env)
    }),
    commonjs(),
    uglify({
      compress: {
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        warnings: true
      }
    })
  ],
  external: [
    // 'mc-scrollto'
  ]
};

export default config;

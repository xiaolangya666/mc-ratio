import '../css/demo.scss';
// 代码着色插件
import McTinting from '@fekit/mc-tinting';

new McTinting({
  theme: 'vscode',
  number: 1
});
